\begin{thebibliography}{10}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }

\bibitem{Allen2013}
Allen, G.I.: {Automatic Feature Selection via Weighted Kernels and
  Regularization}. Journal of Computational and Graphical Statistics  (2013)

\bibitem{Bach2008}
Bach, F.: {Consistency of the group lasso and multiple kernel learning}.
  Journal of Machine Learning Research  (2008)

\bibitem{Bach2009a}
Bach, F.: {High-Dimensional Non-Linear Variable Selection through Hierarchical
  Kernel Learning}. ArXiv 0909.0844  (2009)

\bibitem{Beck2009a}
Beck, A., Teboulle, M.: {A Fast Iterative Shrinkage-Thresholding Algorithm for
  Linear Inverse Problems}. SIAM Journal on Imaging Sciences  (2009)

\bibitem{Bolon-Canedo2013}
Bol{\'{o}}n-Canedo, V., S{\'{a}}nchez-Maro{\~{n}}o, N., Alonso-Betanzos, A.: {A
  review of feature selection methods on synthetic data}. Knowledge and
  Information Systems  (2013)

\bibitem{Bolon-Canedo2015}
Bol{\'{o}}n-Canedo, V., S{\'{a}}nchez-Maro{\~{n}}o, N., Alonso-Betanzos, A.:
  {Recent advances and emerging challenges of feature selection in the context
  of big data}. Knowledge-Based Systems  (2015)

\bibitem{Chan2007}
Chan, A.B., Vasconcelos, N., Lanckriet, G.R.G.: {Direct convex relaxations of
  sparse SVM}. In: International Conference on Machine Learning (2007) (2007)

\bibitem{Chen2017}
Chen, J., Stern, M., Wainwright, M.J., Jordan, M.I.: {Kernel Feature Selection
  via Conditional Covariance Minimization}. Advances in Neural Information
  Processing Systems (NIPS)  (2017)

\bibitem{Fukumizu2012}
Fukumizu, K., Leng, C.: {Gradient-based kernel method for feature extraction
  and variable selection}. In: Advances in Neural Information Processing
  Systems (NIPS) (2012)

\bibitem{Grandvalet2002}
Grandvalet, Y., Canu, S.: {Adaptive scaling for feature selection in SVMs}. In:
  Advances in Neural Information Processing Systems (NIPS) (2002)

\bibitem{Gregorova2018}
Gregorov{\'{a}}, M., Kalousis, A., Marchand-Maillet, S.: {Structured nonlinear
  variable selection}. In: Conference on Uncertainty in Artificial Intelligence
  (UAI) (2018)

\bibitem{Gretton2008}
Gretton, A., Fukumizu, K., Teo, C.H., Song, L., Sch{\"{o}}lkopf, B., Smola,
  A.J.: {A kernel statistical test of independence}. In: Advances in Neural
  Information Processing Systems (NIPS) (2008)

\bibitem{Gurram2014}
Gurram, P., Kwon, H.: {Optimal sparse kernel learning in the empirical kernel
  feature space for hyperspectral classification}. IEEE Journal of Selected
  Topics in Applied Earth Observations and Remote Sensing  (2014)

\bibitem{Guyon2002}
Guyon, I., Weston, J., Barnhill, S., Vapnik, V.: {Gene Selection for Cancer
  Classification using Support Vector Machines}. Machine Learning  (2002)

\bibitem{Hastie1990}
Hastie, T., Tibshirani, R.: {Generalized additive models}. Chapman and Hall
  (1990)

\bibitem{Hastie2015}
Hastie, T., Tibshirani, R., Wainwright, M.: {Statistical Learning with
  Sparsity: The Lasso and Generalizations}. CRC Press (2015)

\bibitem{Kingma2014}
Kingma, D.P., Welling, M.: {Auto-Encoding Variational Bayes}. In: International
  Conference on Learning Representations (ICLR) (2014)

\bibitem{Kohavi1997}
Kohavi, R., John, G.H.: {Wrappers for feature subset selection}. Artificial
  Intelligence  (1997)

\bibitem{Koltchinskii2010}
Koltchinskii, V., Yuan, M.: {Sparsity in multiple kernel learning}. Annals of
  Statistics  (2010)

\bibitem{Lin2006}
Lin, Y., Zhang, H.H.: {Component selection and smoothing in multivariate
  nonparametric regression}. Annals of Statistics  (2006)

\bibitem{Maldonado2011}
Maldonado, S., Weber, R., Basak, J.: {Simultaneous feature selection and
  classification using kernel-penalized support vector machines}. Information
  Sciences  (2011)

\bibitem{Mosci2010}
Mosci, S., Rosasco, L., Santoro, M., Verri, A., Villa, S.: {Solving structured
  sparsity regularization with proximal methods}. In: European Conference on
  Machine Learning and Principles and Practice of Knowledge Discovery in
  Databases (ECML/PKDD) (2010)

\bibitem{Muandet2016}
Muandet, K., Fukumizu, K., Sriperumbudur, B., Sch{\"{o}}lkopf, B.: {Kernel Mean
  Embedding of Distributions: A Review and Beyond}. Foundations and Trends in
  Machine Learning  (2017)

\bibitem{Rahimi2007}
Rahimi, A., Recht, B.: {Random features for large-scale kernel machines}. In:
  Advances in Neural Information Processing Systems (NIPS) (2007)

\bibitem{Rakotomamonjy2003}
Rakotomamonjy, A.: {Variable Selection Using SVM-based Criteria}. Journal
  ofMachine Learning Research  (2003)

\bibitem{Ravikumar2007}
Ravikumar, P., Liu, H., Lafferty, J., Wasserman, L.: {Spam: Sparse additive
  models}. In: Advances in Neural Information Processing Systems (NIPS) (2007)

\bibitem{Ren2015}
Ren, S., Huang, S., Onofrey, J.A., Papademetris, X., Qian, X.: {A Scalable
  Algorithm for Structured Kernel Feature Selection.} Aistats  (2015)

\bibitem{Rosasco2013}
Rosasco, L., Villa, S., Mosci, S.: {Nonparametric sparsity and regularization}.
  Journal of Machine Learning Research  (2013)

\bibitem{Scholkopf2002}
Sch{\"{o}}lkopf, B., Smola, A.J.: {Learning with kernels}. The MIT Press (2002)

\bibitem{Song2007}
Song, L., Smola, A., Gretton, A., Borgwardt, K.M., Bedo, J.: {Supervised
  feature selection via dependence estimation}. Proceedings of the 24th
  international conference on Machine learning - ICML '07  (2007)

\bibitem{Tyagi2016}
Tyagi, H., Krause, A., Eth, Z.: {Efficient Sampling for Learning Sparse
  Additive Models in High Dimensions}. International Conference on Artificial
  Intelligence and Statistics (AISTATS)  (2016)

\bibitem{Weston2003}
Weston, J., Elisseeff, A., Scholkopf, B., Tipping, M.: {Use of the Zero-Norm
  with Linear Models and Kernel Methods}. Journal of Machine Learning Research
  (2003)

\bibitem{Yamada2014}
Yamada, M., Jitkrittum, W., Sigal, L., Xing, E.P., Sugiyama, M.:
  {High-dimensional feature selection by feature-wise kernelized Lasso.} Neural
  Computation  (2014)

\bibitem{Yin2012}
Yin, J., Chen, X., Xing, E.P.: {Group Sparse Additive Models}. In:
  International Conference on Machine Learning (ICML) (2012)

\bibitem{Zhao2014}
Zhao, T., Li, X., Liu, H., Roeder, K.: {CRAN - Package SAM}  (2014)

\bibitem{Zhou2008}
Zhou, D.X.: {Derivative reproducing properties for kernel methods in learning
  theory}. Journal of Computational and Applied Mathematics  (2008)

\end{thebibliography}
