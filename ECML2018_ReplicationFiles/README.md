# ECML2018 replication files

Tools to replicate experiments in ECML2018 paper 'Gregorova et al.,Large-scale Nonlinear Variable Selection via Kernel Random Features'

## Generate experimental data
**This uses Matlab!**

### Synthetic experiments

`matlab < generateSyntheticData.m`

### Real data experiments
Download *ComputerActivity*, *Ailerons*, *Elevators* from http://www.dcc.fc.up.pt/~ltorgo/Regression/DataSets.html. Create mat file for each (concatenate xxx.data and xxx.test into one xxx.mat) in your preferred `realPath`.

Download *mining* from https://www.kaggle.com/edumagalhaes/quality-prediction-in-a-mining-process and create mat file in your preferred `realPath`.

Edit `realPath` in `generateRealData.m`

`matlab < generateRealData.m`

## Run experiments

For example
```
# SRFF method
python main.py --exp-name="SE1_1000_001" --kernel-param=4
# RFF method
python main.py --exp-name="SE1_1000_001" --kernel-param=4 --output-dir="./Experiments/Results_rff" --use-rf
```
where `1000` is the training size and `001`  is the replication number.

Settings of `--kernel-param` :

|Exp Code| kernel param  |
|--|--|
|SE1|4|
|SE2|10|
|SE3|30|
|RCP|3|
|REL|3|
|RAI|4|
|RMN|3|

## Get summary results

Set experiment specifications in `getExpResults.m`

`matlab < getExpResults.m`
