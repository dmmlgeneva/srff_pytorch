function RMN_generateData(dataFolder,expId,trainSize,testSize)
% RMN_GENERATEDATA - generate ore mining dataset
%
% INPUTS
%   dataFolder - folder with the
%   expId - integer experiment Id
%   trainSize - integer size of training sample
%   testSize - integer size of test sample (validation will be the same size)
%
% EXAMPLE: RMN_generateData(1,50,1000)
%
% CREATED: MG 4/4/2018

%% reproducible random data
rng(expId*trainSize)
n = trainSize+2*testSize;

%% load data
load(fullfile(dataFolder,'mining.mat'))
% drop missing column
allData=mining;
% center all data
allData = bsxfun(@minus,allData,mean(allData));
% normalize all data
allData(:,1:end) = bsxfun(@rdivide,allData(:,1:end),std(allData(:,1:end)));

%% inputs and outpus
x = allData(:,1:end-2); % drop Iron Concetrate as per the kaggel request
y = allData(:,end);

%% split to train and test
permIdx = randperm(n);
train.X = x(permIdx(1:trainSize),:);
train.y = y(permIdx(1:trainSize));
valid.X = x(permIdx(trainSize+1:trainSize+testSize),:);
valid.y = y(permIdx(trainSize+1:trainSize+testSize));
test.X = x(permIdx(end-testSize+1:end),:);
test.y = y(permIdx(end-testSize+1:end));
% center outputs by train mean
means.y = mean(train.y);
train.y = train.y-means.y;
valid.y = valid.y-means.y;
test.y = test.y-means.y;

%% save experimental data
if ~exist('Experiments/Data','dir') mkdir('Experiments/Data'); end
dirName = ['Experiments/Data/RMN_',num2str(trainSize,'%03d'),'_',num2str(expId,'%03d')];
mkdir(dirName); 
save([dirName,'/allData'],'train','test','valid','means');
csvwrite([dirName,'/trainX.csv'],train.X);
csvwrite([dirName,'/trainy.csv'],train.y);
csvwrite([dirName,'/validX.csv'],valid.X);
csvwrite([dirName,'/validy.csv'],valid.y);
csvwrite([dirName,'/testX.csv'],test.X);
csvwrite([dirName,'/testy.csv'],test.y);

end