%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% MG 3/4/2018                                                         %%%%%
%%%% generate synthetic datasets for replication of ECML2018 experimetns %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% move back to the main directory
cd ..
addpath(genpath('./'));

%% Synthetic experiments
for trainSize = [1000 5000 10000 50000]
  for rep=1:30
    SE1_generateData(rep,trainSize,1000)
    SE2_generateData(rep,trainSize,1000)
    SE3_generateData(rep,trainSize,1000)
  end
end


