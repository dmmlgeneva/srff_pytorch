function SE1_generateData(expId,trainSize,testSize)
% SE1_GENERATEDATA - generate SE1 synthetic datasets - explore group lasso
%
% INPUTS
%   expId - integer experiment Id
%   trainSize - integer size of training sample
%   testSize - integer size of test sample (validation will be the same size)
%
% EXAMPLE: SE1_generateData(1,50,1000)
%
% CREATED: MG 5/5/2018

%% reproducible random data
rng(2018*expId*trainSize/1000)

n = trainSize+2*testSize;

%% inputs
x = normrnd(0,1,n,18);
%% outputs
y = sin(x(:,7).*x(:,8).*x(:,9)).*sin((x(:,1)+x(:,3)).^2);
y = y + normrnd(0,0.1,n,1);

%% split to train and test
permIdx = randperm(n);
train.X = x(permIdx(1:trainSize),:);
train.y = y(permIdx(1:trainSize));
valid.X = x(permIdx(trainSize+1:trainSize+testSize),:);
valid.y = y(permIdx(trainSize+1:trainSize+testSize));
test.X = x(permIdx(end-testSize+1:end),:);
test.y = y(permIdx(end-testSize+1:end));
% center outputs by train mean
means.y = mean(train.y);
train.y = train.y-means.y;
valid.y = valid.y-means.y;
test.y = test.y-means.y;

%% save experimental data
if ~exist('Experiments/Data','dir') mkdir('Experiments/Data'); end
dirName = ['Experiments/Data/SE1_',num2str(trainSize,'%03d'),'_',num2str(expId,'%03d')];
mkdir(dirName); 
save([dirName,'/allData'],'train','test','valid','means');
csvwrite([dirName,'/trainX.csv'],train.X);
csvwrite([dirName,'/trainy.csv'],train.y);
csvwrite([dirName,'/validX.csv'],valid.X);
csvwrite([dirName,'/validy.csv'],valid.y);
csvwrite([dirName,'/testX.csv'],test.X);
csvwrite([dirName,'/testy.csv'],test.y);

end