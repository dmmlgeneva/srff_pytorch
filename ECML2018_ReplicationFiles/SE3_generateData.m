function SE3_generateData(expId,trainSize,testSize)
% SE3_GENERATEDATA - generate SE3 synthetic datasets - explore group lasso
%
% INPUTS
%   expId - integer experiment Id
%   trainSize - integer size of training sample
%   testSize - integer size of test sample (validation will be the same size)
%
% EXAMPLE: SE3_generateData(1,50,1000)
%
% CREATED: MG 9/4/2018

%% reproducible random data
rng(2018*expId*trainSize)

n = trainSize+2*testSize;

%% inputs
z = normrnd(0,1,n,200);
x = kron(z,ones(1,5)) + normrnd(0,0.1,n,200*5);
%% outputs (same as Ex1)
y = zeros(n,1);
y = 10*(z(:,1).^2 + z(:,3).^2).*exp(-2*(z(:,1).^2 + z(:,3).^2));
errs = normrnd(0,0.01,n,1);
y = y + errs;

%% split to train and test
permIdx = randperm(n);
train.X = x(permIdx(1:trainSize),:);
train.y = y(permIdx(1:trainSize));
valid.X = x(permIdx(trainSize+1:trainSize+testSize),:);
valid.y = y(permIdx(trainSize+1:trainSize+testSize));
test.X = x(permIdx(end-testSize+1:end),:);
test.y = y(permIdx(end-testSize+1:end));
% center outputs by train mean
means.y = mean(train.y);
train.y = train.y-means.y;
valid.y = valid.y-means.y;
test.y = test.y-means.y;

%% save experimental data
if ~exist('Experiments/Data','dir') mkdir('Experiments/Data'); end
dirName = ['Experiments/Data/SE3_',num2str(trainSize,'%03d'),'_',num2str(expId,'%03d')];
mkdir(dirName); 
save([dirName,'/allData'],'train','test','valid','means');
csvwrite([dirName,'/trainX.csv'],train.X);
csvwrite([dirName,'/trainy.csv'],train.y);
csvwrite([dirName,'/validX.csv'],valid.X);
csvwrite([dirName,'/validy.csv'],valid.y);
csvwrite([dirName,'/testX.csv'],test.X);
csvwrite([dirName,'/testy.csv'],test.y);

end