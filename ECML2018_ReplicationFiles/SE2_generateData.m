function SE2_generateData(expId,trainSize,testSize)
% SE2_GENERATEDATA - generate SE2 synthetic datasets - explore enet
%
% INPUTS
%   expId - integer experiment Id
%   trainSize - integer size of training sample
%   testSize - integer size of test sample (validation will be the same size)
%
% EXAMPLE: SE2_generateData(1,50,1000)
%
% CREATED: MG 3/4/2018

%% reproducible random data
rng(expId*trainSize)

n = trainSize+2*testSize;

%% inputs
x = normrnd(0,1,n,100);
%% outputs (same as Ex1)
y = zeros(n,1);
%a = (sum(x(:,1:5),2).^2);
b = (sum(x(:,11:15),2).^2);
y = log( b );
errs = normrnd(0,0.1,n,1);
y = y + errs;
%scatter3(a, b, y, [], y)

%% split to train and test
permIdx = randperm(n);
train.X = x(permIdx(1:trainSize),:);
train.y = y(permIdx(1:trainSize));
train.errs = errs(permIdx(1:trainSize));
valid.X = x(permIdx(trainSize+1:trainSize+testSize),:);
valid.y = y(permIdx(trainSize+1:trainSize+testSize));
valid.errs = errs(permIdx(trainSize+1:trainSize+testSize));
test.X = x(permIdx(end-testSize+1:end),:);
test.y = y(permIdx(end-testSize+1:end));
test.errs = errs(permIdx(end-testSize+1:end));
% center outputs by train mean
means.y = mean(train.y);
train.y = train.y-means.y;
valid.y = valid.y-means.y;
test.y = test.y-means.y;

%% save experimental data
if ~exist('Experiments/Data','dir') mkdir('Experiments/Data'); end
dirName = ['Experiments/Data/SE2_',num2str(trainSize,'%03d'),'_',num2str(expId,'%03d')];
mkdir(dirName); 
save([dirName,'/allData'],'train','test','valid','means');
csvwrite([dirName,'/trainX.csv'],train.X);
csvwrite([dirName,'/trainy.csv'],train.y);
csvwrite([dirName,'/validX.csv'],valid.X);
csvwrite([dirName,'/validy.csv'],valid.y);
csvwrite([dirName,'/testX.csv'],test.X);
csvwrite([dirName,'/testy.csv'],test.y);

end