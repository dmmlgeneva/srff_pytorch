%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% MG 07/04/2018                           %%%%%
%%%% summary of experiment results           %%%%%
%%%% adapted for csv files from SRFF_pytorch %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
%% move back to the main directory
cd ..
addpath(genpath('./'));


%% experiment specification
expCode='RMN';                          % as in the SRFF ECML paper
%trainSizes = [1000 5000 10000 50000];  % for synthetic experiments
trainSizes = [6000];                    % for real experiments as in the paper
reps = 30;                              % nubmer of replications (paper used 30)
methodType = 'srff';                    % methodType either srff or rf
%methodType = 'rff';

%% summary results
RMSE = zeros(length(trainSizes),1);
sRMSE = zeros(length(trainSizes),1);
errs = zeros(length(trainSizes),reps);
% get the summary results
for tS = 1:length(trainSizes)
  for rep=1:reps
    expName = [expCode,'_',num2str(trainSizes(tS),'%03d'),'_',num2str(rep,'%03d')]
    if strcmp('rff',methodType)
      errs(tS,rep) = csvread(fullfile('/Experiments/Results_rff',expName,'test_error.csv'));
    else
      errs(tS,rep) = csvread(fullfile('/Experiments/Results',expName,'test_error.csv'));
      gamma(rep,:) = csvread(fullfile('/Experiments/Results',expName,'test_gamma.csv'));
    end
  end
  if ~strcmp('rf',methodType)
    medSparsity(tS,:) = median(gamma);
  end
end
% get averages across resamples
RMSE(:,1) = mean(sqrt(errs),2);
sRMSE(:,1) = std(sqrt(errs),[],2);
fName = ['Experiments/printOuts/',methodType,'_',expName,'.mat'];
if strcmp('rf',methodType)
  save(fName,'RMSE','sRMSE','errs');
else 
  save(fName,'RMSE','sRMSE','medSparsity','errs','gamma');
end
