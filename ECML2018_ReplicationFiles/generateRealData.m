%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% MG 3/4/2018                                               %%%%%
%%%% generate datasets for replication of ECML2018 experimetns %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% set path to real datasets
realPath = '../Datasets'

%% move back to the main directory
cd ..
addpath(genpath('./'));


%% Real data experiments
for rep=1:30
 REL_generateData(realPath,rep,6000,1000)
 RAI_generateData(realPath,rep,5000,1000)
 RMN_generateData(realPath,rep,50000,10000)
 RCP_generateData(realPath,rep,6000,1000)
end

