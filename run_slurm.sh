#!/bin/bash -l

#SBATCH --job-name=gammafista
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=shared-gpu
#SBATCH --time=12:00:00
#SBATCH --gres=gpu:pascal:1
#SBATCH --constraint="COMPUTE_CAPABILITY_6_0|COMPUTE_CAPABILITY_6_1"
#SBATCH --mem-per-cpu=16000


# Eg: to run many of these do: for i in {0..10} ; do sbatch run_slurm.sh $i ; done
srun $HOME/.venv3/bin/python main.py --input-dir="./E1_10000_001" --exp-name=gammafista$1
