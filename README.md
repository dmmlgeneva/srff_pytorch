# SRFF Pytorch

An implementation of SRFF (Sparse Random Fourier Features algorithm, Gregorova et al. 2018) in pytorch

## Installation

```bash
curl -L -O https://repo.continuum.io/archive/Anaconda3-5.1.0-Linux-x86_64.sh && sh ./Anaconda3-5.1.0-Linux-x86_64.sh # say yes to add to .bashrc
# restart your terminal , i.e. exit, open again

# for CPU version install like such:
conda install pytorch-cpu torchvision -c pytorch

# or for GPU version (using cuda 9.0) install as such (NOTE: you just need to install cpu **OR** gpu; gpu has CPU ops as well) :
conda install pytorch torchvision cuda90 -c pytorch

# clone the repo **WITH ALL THE SUBMODULES!**
git clone --recursive git@bitbucket.org:dmmlgeneva/srf_pytorch.git
```


## Usage

`python main.py --<FLAG>=VALUE`

where the flags are defined as :

```python
# Task parameters
parser.add_argument('--use-rf', action='store_true', default=False,
                    help='run baseline RF instead of SRF (default: False)')
parser.add_argument('--use-ista', action='store_true', default=False,
                    help='run gamma-ista instead of gamma-fista (default: False)')
parser.add_argument('--exp-name', type=str, default="",
                    help="add a custom experiment name (default: None)")
parser.add_argument('--input-dir', type=str, default="./Experiments/Data",
                    help="input directory (default: ./Experiments/Data)")
parser.add_argument('--output-dir', type=str, default="./Experiments/Results",
                    help="output result directory (default: ./Experiments/Data)")
parser.add_argument('--kernel-type', type=str, default="gauss",
                    help="""kernel type : [pol, polhom, gauss, lin] (default: gauss)""")
parser.add_argument('--kernel-param', type=float, default=1.0, help="param for kernel (default: 1.0)")
parser.add_argument('--lambda-min', type=float, default=-9.8, help="min range val for lambda search (default: -9.8)")
parser.add_argument('--lambda-max', type=float, default=0.0, help="max range val for lambda search (default: 0.0)")
parser.add_argument('--lambda-step', type=float, default=0.2, help="step value for lambda search (default: 0.2)")
parser.add_argument('--update-thresh', type=float, default=1e-5, help="update threshold (default: 1e-5)")
parser.add_argument('--num-avg-samples', type=float, default=5,
                    help="how many samples to avg over for exit condition (default: 5)")
parser.add_argument('--num-features', type=int, default=300, help="number of features to use (default: 300)")
parser.add_argument('--max-srf-algo-iter', type=int, default=1000, help="max iters for main srf algo (default: 1000)")
parser.add_argument('--max-gamma-solver-iter', type=int, default=1000, help="max iters for gamma-solver (default: 1000)")
parser.add_argument('--debug', action='store_true', default=False,
                    help='print debug information on sizing, etc (default: False)')

# Optional parameters mainly used for testing
parser.add_argument('--use-manual-grad', action='store_true', default=False,
                    help='uses manual gradFunc instead of autograd (default: False)')
parser.add_argument('--use-steepest-descent', action='store_true', default=False,
                    help='uses steepest descent instead of LU solver (default: False)')

# Device parameters
parser.add_argument('--seed', type=int, default=None,
                    help='seed for numpy and pytorch (default: None)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')

```
