import os
import json
import math
import time
import torch
import pprint
import argparse
import numpy as np
import pandas as pd

from torch.autograd import Variable
from helpers.utils import float_type, to_data, \
    check_or_create_dir, zeros_like, expand_dims, \
    append_to_csv, ones, zeros, uniform, normal, eye

parser = argparse.ArgumentParser(description='SRF Pytorch')

# Task parameters
parser.add_argument('--linfty-ball', action='store_true', default=False,
                    help='project sqrt(gamm) on linfty_ball instead of gamma on simplex (default: False)')
parser.add_argument('--l2-ball', action='store_true', default=False,
                    help='project sqrt(gamm) on l2_ball instead of gamma on simplex (default: False)')
parser.add_argument('--simplex-novec', action='store_true', default=False,
                    help='run nonvectorised simplex-project instead of simplex-project-vectorised (default: False)')
parser.add_argument('--use-rf', action='store_true', default=False,
                    help='run baseline RF instead of SRF (default: False)')
parser.add_argument('--use-ista', action='store_true', default=False,
                    help='run gamma-ista instead of gamma-fista (default: False)')
parser.add_argument('--exp-name', type=str, default="",
                    help="add a custom experiment name (default: None)")
parser.add_argument('--input-dir', type=str, default="./Experiments/Data",
                    help="input directory (default: ./Experiments/Data)")
parser.add_argument('--output-dir', type=str, default="./Experiments/Results",
                    help="output result directory (default: ./Experiments/Data)")
parser.add_argument('--kernel-type', type=str, default="gauss",
                    help="""kernel type : [pol, polhom, gauss, lin] (default: gauss)""")
parser.add_argument('--kernel-param', type=float, default=1.0, help="param for kernel (default: 1.0)")
parser.add_argument('--lambda-min', type=float, default=-9.8, help="min range val for lambda search (default: -9.8)")
parser.add_argument('--lambda-max', type=float, default=0.0, help="max range val for lambda search (default: 0.0)")
parser.add_argument('--lambda-step', type=float, default=0.2, help="step value for lambda search (default: 0.2)")
parser.add_argument('--update-thresh', type=float, default=1e-5, help="update threshold (default: 1e-5)")
parser.add_argument('--num-avg-samples', type=float, default=5,
                    help="how many samples to avg over for exit condition (default: 5)")
parser.add_argument('--num-features', type=int, default=300, help="number of features to use (default: 300)")
parser.add_argument('--max-srf-algo-iter', type=int, default=1000, help="max iters for main srf algo (default: 1000)")
parser.add_argument('--max-gamma-solver-iter', type=int, default=100, help="max iters for gamma-solver (default: 100)")
parser.add_argument('--debug', action='store_true', default=False,
                    help='print debug information on sizing, etc (default: False)')

# Optional parameters mainly used for testing
parser.add_argument('--use-manual-grad', action='store_true', default=False,
                    help='uses manual gradFunc instead of autograd (default: False)')
parser.add_argument('--use-steepest-descent', action='store_true', default=False,
                    help='uses steepest descent instead of LU solver (default: False)')

# Device parameters
parser.add_argument('--seed', type=int, default=None,
                    help='seed for numpy and pytorch (default: None)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()


def load_data(args):
    ''' read the csv files and typecast to fp32 as fp64 is SLOOOOOOOW'''
    base_path = os.path.join(args.input_dir, args.exp_name)
    train_x = pd.read_csv(os.path.join(base_path, 'trainX.csv'), header=None).values.astype(np.float32)
    train_y = pd.read_csv(os.path.join(base_path, 'trainy.csv'), header=None).values.astype(np.float32)
    valid_x = pd.read_csv(os.path.join(base_path, 'validX.csv'), header=None).values.astype(np.float32)
    valid_y = pd.read_csv(os.path.join(base_path, 'validy.csv'), header=None).values.astype(np.float32)
    test_x = pd.read_csv(os.path.join(base_path, 'testX.csv'), header=None).values.astype(np.float32)
    test_y = pd.read_csv(os.path.join(base_path, 'testy.csv'), header=None).values.astype(np.float32)

    # returns a map with gpu-ized tensors's
    return {
        'train': {
            'x': torch.from_numpy(train_x).cuda() if args.cuda else torch.from_numpy(train_x),
            'y': torch.from_numpy(train_y).cuda() if args.cuda else torch.from_numpy(train_y)
        },
        'valid': {
            'x': torch.from_numpy(valid_x).cuda() if args.cuda else torch.from_numpy(valid_x),
            'y': torch.from_numpy(valid_y).cuda() if args.cuda else torch.from_numpy(valid_y)
        },
        'test': {
            'x': torch.from_numpy(test_x).cuda() if args.cuda else torch.from_numpy(test_x),
            'y': torch.from_numpy(test_y).cuda() if args.cuda else torch.from_numpy(test_y)
        }
    }


def l2_loss(preds, targets, reduce_mean=True):
    diff = preds - targets
    reduce_fn = torch.mean if reduce_mean else torch.sum
    return reduce_fn(diff*diff)

def solve_lin_eqn(A, b,  cuda=False):
    ''' use LU to solve Ax = b'''
    # w, _ = torch.gesv(b.cpu(), A.cpu())
    # w = w.cuda() if args.cuda else w
    w, _ = torch.gesv(b, A)
    if args.debug:
        print('\nlinear solver loss: {}'.format(l2_loss(A.mm(w), b)))

    return w

def steepest_descent(A, b, init_w, max_iter=1000, update_thresh=1e-5, cuda=False):
    ''' solve linear system by steepest descent - OBSOLETE '''
    w = init_w
    res = (b - A.mm(w))
    for i in range(max_iter):
        Ar = A.mm(res)         # create for speed
        aStep = (torch.transpose(res, 1, 0).mm(res))/(torch.transpose(res, 1, 0).mm(Ar))
        w = w + aStep * res
        resOld = res
        res = resOld - aStep*Ar

        if torch.norm(res - resOld) < update_thresh:
            break

    if args.debug:
        print('\nlinear solver loss: {}'.format(torch.norm(res)))

    return w


def gamma_fista_loss(g_temp, X, B, y, a, epsilon, n):
    ''' for l2_ball and linfty_ball g_temp is sqaure root of gamma '''
    if args.l2_ball or args.linfty_ball:
        omg = epsilon.mm(torch.diag(torch.mul(g_temp, g_temp)))
    else:
        omg = epsilon.mm(torch.diag(g_temp))
    Z = torch.cos(X.mm(torch.transpose(omg, 1, 0)) + B)
    diff = y - Z.mm(a)
    norm_diff = torch.norm(diff.squeeze())
    return  (0.5/n)*(norm_diff * norm_diff)


def gamma_ista(X, y, init_g, a, epsilon, B, constrain_size):
    ''' for l2_ball and linfty_ball init_g and out_g are sqaure root of gamma '''
    n, d = list(X.size())
    obj_history = np.zeros(args.max_gamma_solver_iter, dtype=np.float32)

    # initiate grad descent motoring
    out_g = to_data(init_g.clone())
    obj_history[0] = gamma_fista_loss(out_g, X, B, y, a, epsilon, n)

    # initiate ISTA variables
    alpha, beta = 100, 0.5

    # do the ISTA projected grad descent
    for i in range(1, args.max_gamma_solver_iter):
        epsilon, B, X, y, a, out_g = [Variable(to_data(epsilon)), Variable(to_data(B)), Variable(to_data(X)),
                                      Variable(to_data(y)), Variable(to_data(a)),
                                      Variable(to_data(out_g), requires_grad=True)]

        # compute gradient of gY w.r.t. gamma fista loss
        if not args.use_manual_grad: # default uses autograd
            grad_g = torch.autograd.grad(gamma_fista_loss(out_g, X, B, y, a, epsilon, n), out_g)[0]
        else:
            grad_g = grad_func(out_g, d, n, B, y, epsilon, X, a)

        while alpha > args.update_thresh:
            grad_update = gY - alpha * grad_g
            if args.l2_ball:
                gNew = grad_update if torch.norm(grad_update.data) <= constrain_size else constrain_size * grad_update / torch.norm(grad_update)
            elif args.linfty_ball:
                in_ball = torch.le(torch.abs(grad_update), constrain_size).float()
                gNew = torch.mul(in_ball, grad_update)
                gNew = torch.add(gNew, -constrain_size, torch.lt(grad_update, -constrain_size).float())
                gNew = torch.add(gNew, constrain_size, torch.gt(grad_update, constrain_size).float())
            elif args.simplex_novec:
                gNew = simplex_project(torch.unsqueeze(grad_update,1), args=args, simplex_size=constrain_size)
            else:
                gNew = simplex_project_vectorised(torch.unsqueeze(grad_update,1), args=args, simplex_size=constrain_size)
            gDiff = gNew - out_g
            objNew = gamma_fista_loss(gNew, X, B, y, a, epsilon, n)
            objY = gamma_fista_loss(out_g, X, B, y, a, epsilon, n)
            qual = objY + grad_g.dot(gDiff) + 0.5 / alpha * (gDiff.dot(gDiff))

            if objNew.data[0] <= qual.data[0]:
                changedG = 1                                  # updated g
                break
            else:
                changedG = 0                                  # g same as before
                alpha = alpha*beta

        if changedG:
            out_g = gNew
            loss = gamma_fista_loss(out_g, X, B, y, a, epsilon, n)
            obj_history[i] = loss.detach().data[0]
        else:
            obj_history[i] = obj_history[i - 1]

        if i > args.num_avg_samples \
           and sum(obj_history[i - (args.num_avg_samples - 1):i] - obj_history[i]) < args.update_thresh:
            if obj_history[i] - obj_history[0] > args.update_thresh:
                print(obj_history)
                raise Exception('gammaISTA: something fishy obj_history[{}]={} > obj_history[0]={}'.format(
                    i, obj_history[i], obj_history[0]
                ))
            break

    return out_g

def grad_func(gTemp, d, n, B, y, epsilon, X, a):
    ''' just to test if autograd is the problem.
        result: it is not.'''
    gradValue = zeros([d, 1], cuda=args.cuda)
    xgeb = X.mm(torch.diag(gTemp)).mm(torch.transpose(epsilon, 1, 0)) + B
    omgCos = torch.cos(xgeb)
    omgSin = torch.sin(xgeb)
    yZa = torch.transpose(y - omgCos.mm(a), 1, 0)
    for dIdx in range(d):
        xE = expand_dims(X[:, dIdx].contiguous(), 1).mm(
            torch.transpose(expand_dims(epsilon[:, dIdx].contiguous(), 1), 1, 0)
        )
        dZ = omgSin * xE
        gradValue[dIdx] = to_data(yZa.mm(dZ).mm(a) / n)

    return Variable(gradValue.squeeze())


def gamma_fista(X, y, init_g, a, epsilon, B, constrain_size):
    ''' for l2_ball init_g and out_g are sqaure root of gamma '''
    n, d = list(X.size())
    obj_history = np.zeros(args.max_gamma_solver_iter, dtype=np.float32)

    # initiate grad descent motoring
    out_g = to_data(init_g.clone())
    obj_history[0] = gamma_fista_loss(out_g, X, B, y, a, epsilon, n)

    # initiate FISTA variables
    titer, gY, alpha, beta = 1.0, out_g.clone(), 100, 0.5

    # do the accelerated projected grad descent
    for i in range(1, args.max_gamma_solver_iter):
        # wrap our computation graph in Variables to use autograd
        epsilon, gY, B, X, y, a, out_g = [Variable(to_data(epsilon)), Variable(to_data(gY), requires_grad=True),
                                          Variable(to_data(B)), Variable(to_data(X)), Variable(to_data(y)),
                                          Variable(to_data(a)), Variable(to_data(out_g))]

        # compute gradient of gY w.r.t. gamma fista loss
        if not args.use_manual_grad: # default uses autograd
            grad_g = torch.autograd.grad(gamma_fista_loss(gY, X, B, y, a, epsilon, n), gY)[0]
        else:
            grad_g = grad_func(gY, d, n, B, y, epsilon, X, a)

        # sanity check on gradient sizing
        assert grad_g.size() == gY.size(), "grads [{}] do not match tensor [{}]".format(
            grad_g.size(), gY.size()
        )
        if args.debug:
            print("\n\t gY = ", gY.size(),
                  "\n\t d(gY) = ", grad_g.size(),
                  "\n\t norm(d(gY)) = ", torch.norm(grad_g).data[0])

        # main solver loop
        while alpha > args.update_thresh:
            grad_update = gY - alpha * grad_g
            if args.l2_ball:
                gNew = grad_update if torch.norm(grad_update.data) <= constrain_size else constrain_size * grad_update / torch.norm(grad_update)
            elif args.linfty_ball:
                in_ball = torch.le(torch.abs(grad_update), constrain_size).float()
                gNew = torch.mul(in_ball, grad_update)
                gNew = torch.add(gNew, -constrain_size, torch.lt(grad_update, -constrain_size).float())
                gNew = torch.add(gNew, constrain_size, torch.gt(grad_update, constrain_size).float())
            elif args.simplex_novec:
                gNew = simplex_project(torch.unsqueeze(grad_update,1), args=args, simplex_size=constrain_size)
            else:
                gNew = simplex_project_vectorised(torch.unsqueeze(grad_update,1), args=args, simplex_size=constrain_size)
            gDiff = gNew - gY
            objNew = gamma_fista_loss(gNew, X, B, y, a, epsilon, n)
            objY = gamma_fista_loss(gY, X, B, y, a, epsilon, n)
            qual = objY + grad_g.dot(gDiff) + (0.5 / alpha * (gDiff.dot(gDiff)))

            if args.debug:
                print("\n\t gNew = ", gNew.size(),
                      "\n\t gDiff = ", gDiff.size(),
                      "\n\t objNew = ", objNew.size(),
                      "\n\t objY = ", objY.size(),
                      "\n\t qualifier = ", qual.size())

            if objNew.data[0] <= qual.data[0]:
                tNew = 0.5 + math.sqrt(1 + 4.0 * titer * titer)/2  # eq 4.2
                gY = gNew + ((titer-1) / tNew * (gNew - out_g))    # eq. 4.3
                titer, out_g = tNew, gNew.clone()                  #  and update titer and gamma
                changedG = 1                                       # updated g
                break
            else:
                changedG = 0                                       # g same as before
                alpha = alpha*beta

        if changedG:
            loss = gamma_fista_loss(out_g, X, B, y, a, epsilon, n)
            obj_history[i] = loss.detach().data[0]
        else:
            obj_history[i] = obj_history[i - 1]

        if i > args.num_avg_samples \
           and sum(obj_history[i - (args.num_avg_samples - 1):i] - obj_history[i]) < args.update_thresh:
            # sanity check if there is no update throughout
            if obj_history[i] - obj_history[0] > args.update_thresh:
                print(obj_history)
                print('gammaFISTA: something fishy obj_history[{}]={} > obj_history[0]={}'.format(
                    i, obj_history[i], obj_history[0]
                ))

            break

    return out_g

def loss_function(a, y, Z, lbda, n):
    # n = Z.size(0) ?
    norm_a = torch.norm(a.squeeze())
    diff = y - Z.mm(a)
    norm_diff = torch.norm(diff.squeeze())
    return (0.5 / n)*(norm_diff * norm_diff) + (0.5 * lbda * norm_a * norm_a)


def srf_algo(X, y, lbda, args):
    n, d = list(X.size()) # bad variable naming, what is n, d?
    epsilon = normal([args.num_features, X.size(1)], cuda=args.cuda)
    b = 2 * np.pi * uniform([args.num_features, 1], cuda=args.cuda)
    B = ones([n, 1], cuda=args.cuda).mm(torch.transpose(b, 1, 0))
    gamma = Variable(1 / args.kernel_param * ones([d], cuda=args.cuda))
    omg = epsilon.mm(torch.diag(gamma.data))
    if args.l2_ball:
        gamma_sqrt = torch.sqrt(gamma)
        constrain_size = torch.norm(gamma_sqrt.data)
    elif args.linfty_ball:
        gamma_sqrt = torch.sqrt(gamma)
        constrain_size = torch.max(torch.abs(gamma_sqrt.data))
    else:
        constrain_size = torch.sum(gamma.data)

    if args.debug:
        print("[n, d] = ", [n, d])
        print("sizing: \n\t epsilon = ", epsilon.size(), "\n\t b = ", b.size(),
              "\n\t B = ", B.size(), "\n\t gamma = ",
              gamma.size(), "\n\t omg = ", omg.size())

    # rand features
    Z = torch.cos(X.mm(torch.transpose(omg, 1, 0)) + B)

    # init model params
    num_feat_eye = eye(args.num_features, args.cuda)
    if not args.use_steepest_descent:
        a = solve_lin_eqn(A=(torch.transpose(Z, 1, 0).mm(Z) + n * lbda * num_feat_eye),
                          b=(torch.transpose(Z, 1, 0).mm(y)), cuda=args.cuda)
    else:
        a = steepest_descent(A=(torch.transpose(Z, 1, 0).mm(Z) + n * lbda * num_feat_eye),
                             b=(torch.transpose(Z, 1, 0).mm(y)),
                             init_w=(normal([args.num_features, 1], cuda=args.cuda)),
                             cuda=args.cuda)

    # keep track of objective function
    obj_history = np.zeros(args.max_srf_algo_iter, dtype=np.float32)
    obj_history[0] = loss_function(a, y, Z, lbda, n)

    # main loop
    for i in range(1, args.max_srf_algo_iter):
        # step 1: new rescaling gamma with fixed params a
        if (args.l2_ball or args.linfty_ball) and not args.use_ista: # default uses fista
            gamma_sqrt = gamma_fista(X, y, gamma_sqrt, a, epsilon, B, constrain_size)
            gamma = torch.mul(gamma_sqrt,gamma_sqrt)
        elif (args.l2_ball or args.linfty_ball):
            gamma_sqrt = gamma_ista(X, y, gamma_sqrt, a, epsilon, B, constrain_size)
            gamma = torch.mul(gamma_sqrt,gamma_sqrt)
        elif not args.use_ista: # default uses fista
            gamma = gamma_fista(X, y, gamma, a, epsilon, B, constrain_size)
        else:
            gamma = gamma_ista(X, y, gamma, a, epsilon, B, constrain_size)

        # step2: ridge over rand features with fixed scaling gamma
        omg = to_data(epsilon).mm(torch.diag(to_data(gamma)))
        Z = torch.cos(to_data(X).mm(torch.transpose(omg, 1, 0)) +
                      ones([n, 1], cuda=args.cuda).mm(torch.transpose(to_data(b), 1, 0)))

        # solve for a using a linear solver
        if not args.use_steepest_descent: # default uses LU
            a = solve_lin_eqn(A=(torch.transpose(Z, 1, 0).mm(Z) + n * lbda * num_feat_eye),
                              b=(torch.transpose(Z, 1, 0).mm(y)), cuda=args.cuda)
        else:
            a = steepest_descent(A=(torch.transpose(Z, 1, 0).mm(Z) + n * lbda * num_feat_eye),
                                 b=(torch.transpose(Z, 1, 0).mm(y)), init_w=a, cuda=args.cuda)

        # keep track of the objective history
        obj_history[i] = loss_function(a, y, Z, lbda, n)
        if obj_history[i] > obj_history[i - 1]:
            print('SRF: something fishy obj_history[{}]={} > obj_history[{}]={}\n'.format(
                i, obj_history[i], i-1, obj_history[i]
            ))

        # check convergence using sum of last 'num_avg_samples'
        if i > args.num_avg_samples \
           and sum(obj_history[i - (args.num_avg_samples - 1):i] - obj_history[i]) < args.update_thresh:
            print("update thresh [{}] satisfied at interval {}, exiting...".format(args.update_thresh, i))
            break

    if args.debug:
        print('gamma = ', gamma)

    # return a map of outputs
    return {
        'obj_history': obj_history,
        'omg': omg,
        'b': b,
        'a': a,
        'gamma': gamma
    }


def predict_linear(X, y, w, reduce_dim=0):
    ''' simply linearly projects and returns mse '''
    # print("z = ", X.size(), " | y = ", y.size(), " w = ", w.size())
    if w.dim() < 2:
        w = expand_dims(w.contiguous(), 1)

    preds = X.mm(w)
    errs = torch.mean((preds - y)**2, reduce_dim)
    return preds, errs


def simplex_project(in_vec, args, simplex_size=1):
    '''
    original func for simplex project as used in SRFF
    Chen, Yunmei, and Xiaojing Ye. Projection onto a simplex. arXiv preprint arXiv:1101.6081 (2011).
    '''
    num_el = len(in_vec)
    if args.debug:
        print("\n\t num_el = ", num_el,
              "\n\t constrain_size = ", simplex_size)

    bget = False
    sorted_vec, _ = torch.sort(in_vec, descending=True)
    tmp_sum, tmax = 0., -np.inf

    for ii in range(1, num_el): # inner loop
        tmp_sum = tmp_sum + sorted_vec[ii - 1]
        tmax = (tmp_sum - simplex_size) / ii
        if tmax.data[0] >= sorted_vec[ii].data[0]:
            bget = True
            break

    if not bget:
        tmax = (tmp_sum + sorted_vec[num_el - 1] - simplex_size) / num_el

    # get the element-wise max against a 0 vector
    elementwise_max = torch.max(in_vec - tmax, zeros_like(in_vec))
    out_vec = elementwise_max

    return torch.squeeze(out_vec)


def simplex_project_vectorised(in_vec, args, simplex_size=1):
    '''
    vectorised version of simplex project with Moreau
    Chen, Yunmei, and Xiaojing Ye. Projection onto a simplex. arXiv preprint arXiv:1101.6081 (2011).
    '''
    num_el = len(in_vec)
    if args.debug:
        print("\n\t num_el = ", num_el,
              "\n\t constrain_size = ", simplex_size)

    sorted_vec, _ = torch.sort(in_vec, descending=True)
    list_idx = torch.arange(1, num_el)
    list_idx = list_idx.cuda() if args.cuda else list_idx
    tmpsum = torch.cumsum(sorted_vec, dim=0)
    tmpmax = (torch.squeeze(tmpsum.data[0:num_el-1]) - simplex_size) / list_idx
    tcheck = torch.ge(tmpmax,  torch.squeeze(sorted_vec.data[1:num_el]))

    if torch.sum(tcheck) > 0:
        tmax_ind = torch.min(torch.masked_select(list_idx, tcheck)) - 1
        tmax = tmpmax[int(tmax_ind)]
    else:
        tmax = (tmpsum[num_el-1] - simplex_size) / num_el

    out_vec = torch.max(in_vec - tmax, zeros_like(in_vec))

    return torch.squeeze(out_vec)

def find_min_valid_err(valid_results_maps):
    ''' takes a list of tensors, concats then on batch and finds min '''
    if isinstance(valid_results_maps, (list, tuple)):
        valid_errors = np.vstack([vm['errors'] for vm in valid_results_maps]).reshape(-1)
    else:
        valid_errors = valid_results_maps['errors']

    min_idx = np.argmin(valid_errors)
    min_val = valid_errors[min_idx]
    return min_val, min_idx


def normest(mat):
    # XXX: version of pytorch on baobab doesnt have magma GPU support
    # if you install it normally it should have GPU SVD
    # _, S, _ = mat.cpu().svd()
    _, S, _ = mat.svd()
    return torch.max(S)


def srf_run(args):
    loader = load_data(args)

    # calculate the norm of the covariance and built a set of lambdas
    sigma = normest(torch.transpose(loader['train']['x'], 1, 0).mm(loader['train']['x']))
    print('estimated sigma = ', sigma)

    # hp search over lambdas
    range_lambdas = torch.arange(start=args.lambda_min, end=args.lambda_max, step=args.lambda_step)
    range_lambdas = range_lambdas.cuda() if args.cuda else range_lambdas
    lambdas = sigma * torch.pow(10, range_lambdas)
    #lambdas = [lambdas[12]]

    # a list of maps. 1 map per lambda
    train_results_map, valid_results_map = [{} for _ in range(len(lambdas))], \
                                           [{} for _ in range(len(lambdas))]

    for i, lambda_val in enumerate(lambdas):    # main train-val loop
        # train and tabulate time
        print("srf : trainining {}: with {}".format(i, lambda_val))
        init_time = time.time()
        train_results_map[i] = srf_algo(loader['train']['x'], loader['train']['y'], lambda_val, args)
        print("srf time: {} ms".format((time.time() - init_time) * 1000.0))

        if args.debug:
            print("solutions: ")
            for tm in train_results_map:
                for k, v in tm.items():
                    if isinstance(v, (torch.cuda.FloatTensor, torch.FloatTensor, Variable)):
                        print("\n\t {} = {}".format(k, v.size()))

        # run validation and store errors in valid_map, TODO: this can probably be a list
        ones_mat = ones([loader['valid']['x'].size(0), 1], cuda=args.cuda)
        Z_valid = torch.cos(loader['valid']['x'].mm(torch.transpose(train_results_map[i]['omg'], 1, 0))
                            + ones_mat.mm(torch.transpose(train_results_map[i]['b'], 1, 0)))
        _, valid_error = predict_linear(Z_valid, loader['valid']['y'], train_results_map[i]['a'])
        valid_results_map[i]['errors'] = np.asarray([valid_error])

    # find the best validation params and build test_results map
    min_val_err, min_val_idx = find_min_valid_err(valid_results_map)
    test_results_map = {
        'lbda_idx': np.array([min_val_idx]),
        'lbda': np.array([lambdas[min_val_idx]]),
        'gamma': train_results_map[min_val_idx]['gamma'],
        'a': train_results_map[min_val_idx]['a'],
        'b': train_results_map[min_val_idx]['b'],
        'omg': train_results_map[min_val_idx]['omg']
    }

    # test results
    ones_mat = ones([loader['test']['x'].size(0), 1], cuda=args.cuda)
    ZTest = torch.cos(loader['test']['x'].mm(torch.transpose(test_results_map['omg'], 1, 0))
                      + ones_mat.mm(torch.transpose(test_results_map['b'], 1, 0)))
    test_preds, test_err = predict_linear(ZTest, loader['test']['y'], test_results_map['a'])
    test_results_map['preds'] = test_preds
    test_results_map['error'] = np.asarray([test_err])

    # save away the results
    save_results(train_results_map, valid_results_map, test_results_map, lambdas)

def save_results(train_results_map, valid_results_map, test_results_map, lambdas):
    ''' helper to take maps and save them to output csvs '''
    def _convert_single_map_to_numpy(single_map):
        for k, v in single_map.items():
            if isinstance(v, (torch.cuda.FloatTensor, torch.FloatTensor, Variable)):
                single_map[k] = to_data(v).cpu().numpy()

        return single_map

    def _convert_to_numpy(map_list):
        if not isinstance(map_list, (tuple, list)):
            return _convert_single_map_to_numpy(map_list)

        for i in range(len(map_list)):
            map_list[i] = _convert_single_map_to_numpy(map_list[i])

        return map_list

    # convert the maps with torch tensors to numpy for saving
    train_results_map = _convert_to_numpy(train_results_map)
    valid_results_map = _convert_to_numpy(valid_results_map)
    test_results_map = _convert_to_numpy(test_results_map)

    # dump results into the output directory
    base_output_dir = os.path.join(args.output_dir, args.exp_name)
    check_or_create_dir(base_output_dir)

    # TODO: fix pandas writing the csv's as strings
    #pd.DataFrame(train_results_map).to_csv(os.path.join(base_output_dir, 'train.csv'), index=False)
    #pd.DataFrame(valid_results_map).to_csv(os.path.join(base_output_dir, 'valid.csv'), index=False)
    #pd.DataFrame(test_results_map).to_csv(os.path.join(args.output_dir, 'test.csv'))

    # for simplicity we save each of the lamba values as a separate CSV
    def _append_csv_with_prefix(single_map, prefix='test'):
        for k, v in single_map.items():
            filename = os.path.join(base_output_dir, "{}_{}.csv".format(prefix, k))
            append_to_csv(v, filename)

    if isinstance(train_results_map, (list, tuple)):
        for i, lbda in enumerate(lambdas):
            _append_csv_with_prefix(train_results_map[i],
                                    prefix='train{}'.format(lbda))
            _append_csv_with_prefix(valid_results_map[i],
                                    prefix='valid{}'.format(lbda))
    else:
        _append_csv_with_prefix(train_results_map, "train")
        _append_csv_with_prefix(valid_results_map, "valid")

    _append_csv_with_prefix(test_results_map)

    # write the configuration as a json
    config_filename = os.path.join(base_output_dir, 'config.json')
    with open(config_filename, 'w') as fp:
        json.dump(vars(args), fp)


def ridge_regpath(X, y, lambdas):
    '''run ridge regression for multiple hp values of lambda'''
    n, d = list(X.size())
    ll = len(lambdas)
    w = zeros([d, ll], cuda=args.cuda)
    if n > d:
        eigenvalues, eigenvectors = X.t().mm(X).symeig(eigenvectors=True)
        VXy = eigenvectors.t().mm(X.t().mm(y))
        for i in range(ll):
            # NOTE: torch returns eigenvalues as a vector, not a matrix
            w[:, i] = eigenvectors.mm(
                torch.diag(1.0 / (eigenvalues + n*lambdas[i])).mm(VXy)
            )
    else:
        eigenvalues, eigenvectors = X.mm(X.t()).symeig(eigenvectors=True)
        XV = X.t().mm(eigenvectors)
        Vy = eigenvectors.t().mm(y)
        for i in range(ll):
            # NOTE: torch returns eigenvalues as a vector, not a matrix
            w[:, i] = XV.mm(torch.diag(1.0 / (eigenvalues + n*lambdas[i]))).mm(Vy)

    return w

def rf_run(args):
    loader = load_data(args)

    # calculate the norm of the covariance and built a set of lambdas
    sigma = normest(torch.transpose(loader['train']['x'], 1, 0).mm(loader['train']['x']))
    print('estimated sigma = ', sigma)

    # hp search over lambdas
    range_lambdas = torch.arange(start=args.lambda_min, end=args.lambda_max, step=args.lambda_step)
    range_lambdas = range_lambdas.cuda() if args.cuda else range_lambdas
    lambdas = sigma * torch.pow(10, range_lambdas)

    # generate random features
    n, d = list(loader['train']['x'].size()) # bad variable naming, what is n, d?
    epsilon = normal([args.num_features, d], cuda=args.cuda)
    b = 2 * np.pi * uniform([args.num_features, 1], cuda=args.cuda)
    B = ones([n, 1], cuda=args.cuda).mm(torch.transpose(b, 1, 0))
    gamma = 1 / args.kernel_param  # TODO: is this supposed to be a scalar?
    omg = gamma*epsilon
    Z_train = torch.cos(loader['train']['x'].mm(torch.transpose(omg, 1, 0)) + B)

    if args.debug:
        print("[n, d] = ", [n, d])
        print("sizing: \n\t epsilon = ", epsilon.size(), "\n\t b = ", b.size(),
              "\n\t B = ", B.size(), "\n\t gamma = ",
              1, "\n\t omg = ", omg.size(), "\n\t Z_train = ", Z_train.size())

    # training
    a_train = ridge_regpath(Z_train, loader['train']['y'], lambdas)
    _, errs_train = predict_linear(Z_train, loader['train']['y'], a_train)
    errs_train = errs_train.cpu().numpy()
    train_results_map = {
        'a': a_train,
        'errors': errs_train
    }

    # validation
    ones_mat = ones([loader['valid']['x'].size(0), 1], cuda=args.cuda)
    Z_valid = torch.cos(loader['valid']['x'].mm(torch.transpose(omg, 1, 0))
                        + ones_mat.mm(torch.transpose(b, 1, 0)))
    _, errs_valid = predict_linear(Z_valid, loader['valid']['y'], a_train)
    errs_valid = errs_valid.cpu().numpy()
    valid_results_map = {
        'errors': errs_valid
    }

    # find the best validation params and build test_results map
    min_val_err, min_val_idx = find_min_valid_err(valid_results_map)

    test_results_map = {
        'lbda_idx': np.array([min_val_idx]),
        'lbda': np.array([lambdas[min_val_idx]]),
        'a': train_results_map['a'][:, min_val_idx],
    }

    # test results, we don't use the best b as above in the Z calc?
    ones_mat = ones([loader['test']['x'].size(0), 1], cuda=args.cuda)
    ZTest = torch.cos(loader['test']['x'].mm(torch.transpose(omg, 1, 0))
                      + ones_mat.mm(torch.transpose(b, 1, 0)))
    test_preds, test_err = predict_linear(ZTest, loader['test']['y'], test_results_map['a'])
    test_results_map['preds'] = test_preds
    test_results_map['error'] = np.asarray([test_err])

    # save away the results
    save_results(train_results_map, valid_results_map, test_results_map, lambdas)


if __name__ == "__main__":
    # handle randomness / non-randomness
    if args.seed is not None:
        print("setting seed %d" % args.seed)
        np.random.seed(args.seed)
        torch.manual_seed_all(args.seed)

    # print out the current config
    print(pprint.PrettyPrinter(indent=4).pformat(vars(args)))

    # random seed for reproducibility
    torch.manual_seed(2018)

    # run either random feature baseline
    # or srf, default is srf
    if args.use_rf:
        rf_run(args)
    else:
        srf_run(args)
